package com.dotsub.backend.service;

import com.dotsub.backend.domain.FileInfo;
import com.dotsub.backend.domain.FileInfoDto;
import com.dotsub.backend.domain.SearchFilesCriteria;
import com.dotsub.backend.exception.RecordNotFoundException;
import com.dotsub.backend.mapper.FileInfoMapper;
import com.dotsub.backend.repository.FileInfoRepository;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FileInfoServiceImpl implements FileInfoService {
	private final FileInfoRepository fileInfoRepository;

	private final FileInfoMapper fileInfoMapper = Mappers.getMapper(FileInfoMapper.class);

	public FileInfoServiceImpl(FileInfoRepository fileInfoRepository) {
		this.fileInfoRepository = fileInfoRepository;
	}

	@Override
	public List<FileInfoDto> getFiles(SearchFilesCriteria criteria) {
		final List<FileInfo> files = new ArrayList<>();

		if (StringUtils.hasText(criteria.getSearchPattern())) {
			files.addAll(fileInfoRepository.findByTitleContainsOrDescriptionContains(
					criteria.getSearchPattern(),
					criteria.getSearchPattern()
			));
		} else {
			files.addAll(fileInfoRepository.findAll());
		}

		return files
				.stream()
				.map(fileInfoMapper::toFileInfoDtoNoBytes)
				.collect(Collectors.toList());
	}

	@Override
	public FileInfoDto getFileById(Long id) {
		return fileInfoRepository.findById(id)
				.map(fileInfoMapper::toFileInfoDto)
				.orElseThrow(RecordNotFoundException::new);
	}

	@Override
	public void deleteFileById(Long id) {
		fileInfoRepository.deleteById(id);
	}

	@Override
	public Long save(FileInfoDto fileInfoDto) {
		return fileInfoRepository.save(fileInfoMapper.toFileInfo(fileInfoDto)).getId();
	}
}
