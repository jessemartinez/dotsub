package com.dotsub.backend.service;

import com.dotsub.backend.domain.FileInfoDto;
import com.dotsub.backend.domain.SearchFilesCriteria;

import java.util.List;

public interface FileInfoService {
	/**
	 * Fetch all files based on a search criteria
	 * @param criteria search criteria
	 * @return list of FileInfoDto matching criteria
	 */
	List<FileInfoDto> getFiles(SearchFilesCriteria criteria);

	/**
	 * Fetch a file based on its id
	 * @param id file id
	 * @return FileInfoDto
	 */
	FileInfoDto getFileById(Long id);

	/**
	 * Deletes a file based on its id
	 * @param id file id
	 */
	void deleteFileById(Long id);

	/**
	 * Savea a file
	 * @param fileInfo file to save
	 * @return persisted file id
	 */
	Long save(FileInfoDto fileInfo);
}
