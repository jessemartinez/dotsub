package com.dotsub.backend.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.time.LocalDateTime;

@Entity
public class FileInfo {
	@Id
	@GeneratedValue
	private Long id;
	@Lob
	private byte[] bytes;
	private String mediaType;
	private String name;
	private String title;
	private String description;
	private LocalDateTime creationDate;

	public FileInfo() {
	}

	public FileInfo(Long id, byte[] bytes, String mediaType, String name, String title,
	                String description, LocalDateTime creationDate) {
		this.id = id;
		this.bytes = bytes;
		this.name = name;
		this.mediaType = mediaType;
		this.title = title;
		this.description = description;
		this.creationDate = creationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMediaType() {
		return mediaType;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}
}
