package com.dotsub.backend.domain;

import javax.validation.constraints.NotEmpty;

public class SaveFileInfoRequest {
	@NotEmpty
	private String title;
	@NotEmpty
	private String description;

	public SaveFileInfoRequest() {
	}

	public SaveFileInfoRequest(@NotEmpty String title, @NotEmpty String description) {
		this.title = title;
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
