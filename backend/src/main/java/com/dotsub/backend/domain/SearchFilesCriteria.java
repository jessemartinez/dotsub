package com.dotsub.backend.domain;

public class SearchFilesCriteria {
	private String searchPattern;

	public SearchFilesCriteria(String searchPattern) {
		this.searchPattern = searchPattern;
	}

	public String getSearchPattern() {
		return searchPattern;
	}

	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
