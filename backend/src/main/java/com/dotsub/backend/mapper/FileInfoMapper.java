package com.dotsub.backend.mapper;

import com.dotsub.backend.domain.FileInfo;
import com.dotsub.backend.domain.FileInfoDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface FileInfoMapper {
	FileInfo toFileInfo(FileInfoDto fileInfoDto);

	FileInfoDto toFileInfoDto(FileInfo fileInfo);

	@Mapping(target = "bytes", ignore = true)
	FileInfoDto toFileInfoDtoNoBytes(FileInfo fileInfo);
}
