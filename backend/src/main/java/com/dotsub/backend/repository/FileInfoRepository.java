package com.dotsub.backend.repository;

import com.dotsub.backend.domain.FileInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileInfoRepository extends JpaRepository<FileInfo, Long> {
	List<FileInfo> findByTitleContainsOrDescriptionContains(String title, String description);
}
