package com.dotsub.backend.controller;

import com.dotsub.backend.domain.FileInfoDto;
import com.dotsub.backend.domain.SaveFileInfoRequest;
import com.dotsub.backend.domain.SearchFilesCriteria;
import com.dotsub.backend.service.FileInfoService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Function;

@RestController
@RequestMapping("/api/files")
public class FileInfoController {
	private final FileInfoService fileInfoService;

	private final Function<String, MediaType> toMediaType = s -> {
		final String[] tokens = s.split("/");
		return new MediaType(tokens[0], tokens[1]);
	};

	public FileInfoController(FileInfoService fileInfoService) {
		this.fileInfoService = fileInfoService;
	}

	@GetMapping()
	public ResponseEntity<List<FileInfoDto>> getFiles(@RequestParam(required = false) String searchPattern) {
		return ResponseEntity.ok(fileInfoService.getFiles(new SearchFilesCriteria(searchPattern)));
	}

	@GetMapping("/{id}/download")
	public ResponseEntity<byte[]> downloadFile(@PathVariable Long id) {
		final FileInfoDto file = fileInfoService.getFileById(id);
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(toMediaType.apply(file.getMediaType()));
		headers.setContentDispositionFormData(file.getName(), file.getName());
		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		headers.setContentLength(file.getBytes().length);
		return new ResponseEntity<>(file.getBytes(), headers, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteFile(@PathVariable Long id) {
		fileInfoService.deleteFileById(id);
		return ResponseEntity.ok().build();
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Long> save(@RequestPart(value = "files") MultipartFile[] files,
	                                 @RequestPart(value = "request") @Valid SaveFileInfoRequest request) throws IOException {
		if (files == null || files.length == 0)
			throw new IllegalArgumentException("Invalid file");

		return ResponseEntity.ok(fileInfoService.save(new FileInfoDto(
				null,
				files[0].getBytes(),
				files[0].getContentType(),
				files[0].getOriginalFilename(),
				request.getTitle(),
				request.getDescription(),
				LocalDateTime.now()
		)));
	}
}
