package com.dotsub.backend.controller;

import com.dotsub.backend.domain.FileInfoDto;
import com.dotsub.backend.domain.SaveFileInfoRequest;
import com.dotsub.backend.exception.RecordNotFoundException;
import com.dotsub.backend.repository.FileInfoRepository;
import com.dotsub.backend.service.FileInfoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class FileInfoControllerTest {
	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	FileInfoRepository fileInfoRepository;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private FileInfoService fileInfoService;

	@AfterEach
	public void resetData() {
		fileInfoRepository.deleteAll();
	}

	@Test
	public void shouldAddFile() throws Exception {
		final FileInfoDto fileInfo = getTextFileInfo();

		final MockMultipartFile file = new MockMultipartFile(
				"files",
				fileInfo.getName(),
				fileInfo.getMediaType(),
				fileInfo.getBytes());

		final MockMultipartFile request = new MockMultipartFile(
				"request",
				"",
				"application/json",
				objectMapper.writeValueAsBytes(
						new SaveFileInfoRequest(fileInfo.getTitle(), fileInfo.getDescription())
				));

		final String fileId = mockMvc.perform(
				multipart("/api/files")
						.file(file)
						.file(request))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse().getContentAsString();

		final FileInfoDto newFile = fileInfoService.getFileById(Long.parseLong(fileId));

		Assertions.assertNotNull(newFile);
	}

	@Test
	public void shouldDownloadFile() throws Exception {
		final Long fileId = fileInfoService.save(getTextFileInfo());

		mockMvc.perform(
				get("/api/files/{id}/download", fileId))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.TEXT_PLAIN_VALUE));
	}

	@Test
	public void shouldDeleteFile() throws Exception {
		final Long fileId = fileInfoService.save(getTextFileInfo());

		mockMvc.perform(
				delete("/api/files/{id}", fileId))
				.andExpect(status().isOk());

		Assertions.assertThrows(RecordNotFoundException.class, () -> fileInfoService.getFileById(fileId));
	}

	private FileInfoDto getTextFileInfo() {
		final FileInfoDto request = new FileInfoDto();
		request.setTitle("Test text file title");
		request.setName("test.txt");
		request.setDescription("Test json file description");
		request.setBytes("hello world".getBytes());
		request.setMediaType("text/plain");
		return request;
	}
}
