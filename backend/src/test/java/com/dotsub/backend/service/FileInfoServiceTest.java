package com.dotsub.backend.service;

import com.dotsub.backend.domain.FileInfoDto;
import com.dotsub.backend.domain.SearchFilesCriteria;
import com.dotsub.backend.exception.RecordNotFoundException;
import com.dotsub.backend.repository.FileInfoRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class FileInfoServiceTest {
	@Autowired
	FileInfoRepository fileInfoRepository;

	@Autowired
	FileInfoService fileInfoService;

	@AfterEach
	public void resetData() {
		fileInfoRepository.deleteAll();
	}

	@Test
	public void shouldReturnFileById() {
		final FileInfoDto textFileInfoDto = getPlainTextFileInfo();

		final Long fileId = fileInfoService.save(textFileInfoDto);
		final FileInfoDto file = fileInfoService.getFileById(fileId);

		Assertions.assertNotNull(file.getId());
		Assertions.assertEquals(textFileInfoDto.getMediaType(), file.getMediaType());
		Assertions.assertEquals(textFileInfoDto.getName(), file.getName());
		Assertions.assertArrayEquals(textFileInfoDto.getBytes(), file.getBytes());
		Assertions.assertEquals(textFileInfoDto.getTitle(), file.getTitle());
		Assertions.assertEquals(textFileInfoDto.getDescription(), file.getDescription());
	}

	@Test
	public void shouldDeleteFile() {
		final Long fileId = fileInfoService.save(getPlainTextFileInfo());
		fileInfoService.deleteFileById(fileId);
		Assertions.assertThrows(RecordNotFoundException.class, () -> fileInfoService.getFileById(fileId));
	}

	@Test
	public void shouldFailFileById() {
		Assertions.assertThrows(RecordNotFoundException.class, () -> fileInfoService.getFileById(-1L));
	}

	@Test
	public void shouldReturnAllFiles() {
		final FileInfoDto textFileInfoDto = getPlainTextFileInfo();
		final FileInfoDto htmlFileInfoDto = getHtmlFileInfo();

		fileInfoService.save(textFileInfoDto);
		fileInfoService.save(htmlFileInfoDto);

		final List<FileInfoDto> files = fileInfoService.getFiles(new SearchFilesCriteria(""));
		Assertions.assertEquals(2, files.size());
		Assertions.assertEquals(textFileInfoDto.getMediaType(), files.get(0).getMediaType());
		Assertions.assertEquals(htmlFileInfoDto.getMediaType(), files.get(1).getMediaType());
	}

	@Test
	public void shouldReturnFilesMatchingCriteria() {
		final FileInfoDto htmlFileInfoDto = getHtmlFileInfo();

		fileInfoService.save(getPlainTextFileInfo());
		fileInfoService.save(htmlFileInfoDto);

		final List<FileInfoDto> files = fileInfoService.getFiles(new SearchFilesCriteria("html file"));
		Assertions.assertEquals(1, files.size());
		Assertions.assertEquals(htmlFileInfoDto.getName(), files.get(0).getName());
		Assertions.assertEquals(htmlFileInfoDto.getMediaType(), files.get(0).getMediaType());
	}

	private FileInfoDto getPlainTextFileInfo() {
		final FileInfoDto request = new FileInfoDto();
		request.setTitle("Test text file title");
		request.setName("test.txt");
		request.setDescription("Test text file description");
		request.setBytes("Hello world".getBytes());
		request.setMediaType("text/plain");
		return request;
	}

	private FileInfoDto getHtmlFileInfo() {
		final FileInfoDto request = new FileInfoDto();
		request.setTitle("Test html file title");
		request.setName("test.html");
		request.setDescription("Test html file description");
		request.setBytes("<html><body>Hello world<body><html>".getBytes());
		request.setMediaType("text/html");
		return request;
	}
}
