import axios from 'axios';
import {FileInfo, SaveFileRequest, SearchFilesRequest} from "./types";

class AppService {
    save(request: SaveFileRequest) {
        let formData = new FormData();
        formData.append("request", new Blob([JSON.stringify(request)], {type: 'application/json'}));
        formData.append("files", request.file)
        return axios.post('/api/files', formData, {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            }
        );
    }

    fetchAll(request: SearchFilesRequest): Promise<Array<FileInfo>> {
        return axios.get<Array<FileInfo>>('/api/files', {
            params: request
        }).then(response => response.data);
    }

    delete(fileId: number) {
        return axios.delete(`/api/files/${fileId}`);
    }
}

export const appService = new AppService()