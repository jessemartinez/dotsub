import React from 'react';
import {act, cleanup, fireEvent, render, screen, waitFor} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import App from './App';
import axios from 'axios';
import {FileInfo} from "./types";

const fakeFileInfo : FileInfo = {
    id: 1,
    name: "test.txt",
    description: "Test description title",
    title: "Test file title",
    creationDate: "2021-02-15T00:00:00"
};

afterEach(cleanup);
beforeEach(() => {
    //axios mocking
    jest.mock("axios");
    jest.spyOn(axios, "get")
        .mockImplementation(() => Promise.resolve({data: [fakeFileInfo]}));
    jest.spyOn(axios, "post")
        .mockImplementation(() => Promise.resolve({data: {}}));
})

test('renders app', async () => {
        render(<App/>);
    const element = screen.getAllByPlaceholderText('Search File by title or description');

    await waitFor(() => {
        expect(element).toBeDefined();
    });
});

test('adds file', async () => {
    render(<App/>);
    let showForm = screen.getByTestId('show-file-form');
    let fileFom = screen.getByTestId('file-form');

    act(() => {
        //show add file form
        showForm.dispatchEvent(new MouseEvent("click", {bubbles: true}));
    });

    //form is visible
    await waitFor(() => {
        expect(fileFom.className).toBe("collapse show")
    });

    let file = new File(['Test text file'], fakeFileInfo.name, {type: "text/plain"})
    let fileFormSave = screen.getByTestId("file-form-save");
    let fileFormFile = screen.getByTestId("file-form-file");
    let fileFormTitle = screen.getByTestId("file-form-title");
    let fileFormDescription = screen.getByTestId("file-form-description");

    act(() => {
        //populate form
        fireEvent.change(fileFormTitle, {target: {value: fakeFileInfo.title}});
        fireEvent.change(fileFormDescription, {target: {value: fakeFileInfo.description}});
        userEvent.upload(fileFormFile, file)
    });

    //form is valid
    expect(fileFormSave).toBeEnabled();

    act(() => {
        //save form
        fileFormSave.dispatchEvent(new MouseEvent("click", {bubbles: true}))
    });

    await waitFor(() => {
        expect(screen.getByTestId("file-id").textContent).toBe(String(fakeFileInfo.id));
        expect(screen.getByTestId("file-title").textContent).toBe(fakeFileInfo.title);
        expect(screen.getByTestId("file-description").textContent).toBe(fakeFileInfo.description);
        expect(screen.getByTestId("file-date").textContent).toBe(fakeFileInfo.creationDate);

        //form is hidden
        expect(fileFom.className).toBe("collapse")
    });
});

