export type SaveFileRequest = {
    file: File,
    name: string,
    title: string,
    description: string
}

export type SearchFilesRequest = {
    searchPattern: string
}

export type FileInfo = {
    id: number,
    name: string,
    title: string,
    description: string
    creationDate: string
}