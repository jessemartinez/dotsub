import React, {useEffect, useState} from 'react';
import {Button, Col, Collapse, Container, Form, InputGroup, Row, Table} from "react-bootstrap";
import {faDownload, faPlusCircle, faSearch, faTrashAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import * as Yup from "yup";
import {useForm} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup";
import {appService} from "./AppService";
import {FileInfo} from './types';

type FormValue = {
    file: File | null,
    title: string,
    description: string
}

const EMPTY_FORM: FormValue = {
    file: null,
    title: '',
    description: ''
}

const schema = Yup.object()
    .shape({
        file: Yup.mixed()
            .transform(function (value, original) {
                return original && original.length > 0 ? original[0] : null;
            }).required("File is required"),
        title: Yup.string()
            .trim().required("File title is required"),
        description: Yup.string()
            .trim().required("File description is required")
    });

const DEFAULT_FILE_NAME = 'Select file';

function App() {
    const [files, setFiles] = useState<Array<FileInfo>>([]);
    const [searchInProgress, setSearchInProgress] = useState(false);
    const [searchPattern, setSearchPattern] = useState('');
    const [fileName, setFileName] = useState(DEFAULT_FILE_NAME);
    const [addFormVisible, setAddFormVisible] = useState(false);
    const [updateCount, setUpdateCount] = useState(0);
    const {register, handleSubmit, errors, formState, reset} = useForm({
        mode: 'onChange',
        resolver: yupResolver(schema),
        defaultValues: EMPTY_FORM
    });
    const fetchData = async () => {
        setSearchInProgress(true);
        const files = await appService.fetchAll({searchPattern: searchPattern});
        setFiles(files);
        setSearchInProgress(false);
    };

    useEffect(() => {
        fetchData();
    }, [updateCount, searchPattern]);

    let resetFileForm = () => {
        setAddFormVisible(false);
        reset();
        setFileName(DEFAULT_FILE_NAME)
    }

    let saveFile = async (form: FormValue) => {
        if (form.file && form.file.name) {
            await appService.save({
                file: form.file as File,
                name: form.file.name,
                title: form.title,
                description: form.description
            });
            setUpdateCount(updateCount + 1);
            resetFileForm();
        }
    }

    let searchFiles = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter') {
            setSearchPattern(event.currentTarget.value);
        }
    }

    let deleteFile = (fileId: number) => {
        appService
            .delete(fileId)
            .then(() => {
                setUpdateCount(updateCount + 1);
            });
    }

    return (
        <Container>
            <Row>
                <Col>
                    <Form.Group>
                        <InputGroup>
                            <InputGroup.Prepend>
                                <InputGroup.Text id="btnGroupAddon">
                                    <FontAwesomeIcon icon={faSearch}/>
                                </InputGroup.Text>
                            </InputGroup.Prepend>
                            <Form.Control
                                placeholder="Search File by title or description"
                                aria-describedby="btnGroupAddon"
                                onKeyPress={searchFiles}
                                name="searchPattern"/>
                        </InputGroup>
                    </Form.Group>
                </Col>
                <Col lg={2} md={2} sm={1} xs={1} className="mr-2 mr-sm-4">
                    <Button variant="success" type="button"
                            disabled={addFormVisible}
                            data-testid="show-file-form"
                            onClick={() => setAddFormVisible(!addFormVisible)}>
                        Add
                    </Button>
                </Col>
            </Row>
            <Collapse in={addFormVisible}>
                <Form data-testid="file-form">
                    <Row>
                        <Col lg={6} md={6} sm={6} className="mt-1">
                            <Form.Group>
                                <Form.File custom>
                                    <Form.File.Input
                                        name="file"
                                        data-testid="file-form-file"
                                        ref={register}
                                        onChange={(e: any) => {
                                            let fileName = e.target.files.length > 0 ? e.target.files[0].name : null;
                                            setFileName(fileName);
                                        }}
                                        isInvalid={errors.file != null}/>
                                    <Form.File.Label data-browse="Browse">
                                        {fileName}
                                    </Form.File.Label>
                                    <Form.Control.Feedback type="invalid">
                                        {errors.file?.message}
                                    </Form.Control.Feedback>
                                </Form.File>
                            </Form.Group>
                        </Col>
                        <Col className="mt-1">
                            <Form.Group>
                                <Form.Control
                                    placeholder="Title"
                                    name="title"
                                    data-testid="file-form-title"
                                    isInvalid={errors.title != null}
                                    ref={register}/>
                                <Form.Control.Feedback type="invalid">
                                    {errors.title?.message}
                                </Form.Control.Feedback>
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Form.Group>
                                <Form.Control
                                    type="email"
                                    placeholder="Description"
                                    name="description"
                                    data-testid="file-form-description"
                                    isInvalid={errors.description != null}
                                    ref={register}/>
                                <Form.Control.Feedback type="invalid">
                                    {errors.description?.message}
                                </Form.Control.Feedback>
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={2} md={3} sm={3} className="mt-1 mb-1">
                            <Button variant="primary"
                                    disabled={!formState.isDirty || !formState.isValid}
                                    onClick={handleSubmit(saveFile)}
                                    data-testid="file-form-save"
                                    block>
                                Save
                            </Button>
                        </Col>
                        <Col lg={2} md={3} sm={3} className="mt-1 mb-1">
                            <Button variant="dark"
                                    onClick={resetFileForm}
                                    data-testid="file-form-cancel"
                                    block>
                                Cancel
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </Collapse>
            <Row className="mt-2">
                <Col>
                    <Table striped hover size="sm">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Date</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        {files.map(file =>
                            <tr key={file.id}>
                                <td data-testid="file-id">{file.id}</td>
                                <td data-testid="file-title">{file.title}</td>
                                <td data-testid="file-description">{file.description}</td>
                                <td data-testid="file-date">{file.creationDate}</td>
                                <td className="fa-pull-right">
                                    <Button size="sm"
                                            href={`/api/files/${file.id}/download`}>
                                        <FontAwesomeIcon icon={faDownload}/>
                                    </Button>
                                    &nbsp;
                                    <Button size="sm"
                                            data-testid="delete-file"
                                            onClick={() => {
                                                deleteFile(file.id)
                                            }} variant="danger">
                                        <FontAwesomeIcon icon={faTrashAlt}/>
                                    </Button>
                                </td>
                            </tr>
                        )}
                        {files.length === 0 &&
                        <tr>
                            <td colSpan={5} className="text-center">
                                {searchInProgress ? 'Searching...' : 'No files found'}
                            </td>
                        </tr>
                        }
                        </tbody>
                    </Table>
                </Col>
            </Row>
        </Container>
    );
}

export default App;
