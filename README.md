# DotSub coding test

## Backend

This application was built using the following frameworks
- Spring Boot
- Spring Data JPA
- Mapstruct
- MockMvc
- Junit 5
- Maven

#### Run
`cd /[project_root]/backend`

`mvn clean test compile spring-boot:run`

Open [http://localhost:18080/api/files](http://localhost:18080/api/files) to view it in the browser.


## Frontend

This application was built using the following frameworks
- yarn
- reactjs
- bootstrap-react
- react-router
- fontawesome
- yup
- react-hook-form
- axios
- jest

#### Initialize dependencies
`cd /[project_root]/frontend`

`yarn`

#### Run unit tests
`cd /[project_root]/frontend`

`yarn test --watchAll`

#### Run
`cd /[project_root]/frontend`

`yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.